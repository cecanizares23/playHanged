import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // Definimos las variables
  letra: string = '';
  nombres: any = [];
  nombreSecreto: any = [];//this.palabraAleatoria(0, (this.nombres.length - 1));
  palabra: any = '';
  muestraHuecos: any; //= this.muestraHuecosPalabra();
  mensaje: string = 'Selecciona una letra del listado.';
  vidas: number = 8;
  puntos: number = 0;
  ganador: number = 0;
  teclado: String = '';
  id: string = "";
  nivel: string = "";
  imagen: any = '';//'../assets/image/0.png';
  awaitingAjaxCall:boolean[] = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];

  //Array para almacenar letras seleccionadas
  controlLetras = new Array;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {

    this.nivel = this.navParams.get('nivel');
    if (this.nivel == "Facil") {
      console.log(this.nivel);
      this.nombres = ['MOTOS', 'CARRO', 'ACERO', 'AMIGO', 'MUJER', 'RADIO', 'PIEDRA', 'COMIDA', 'BLUSAS', 'TESORO', 'ELEVAR', 'OFICIO', 'NAVAJA', 'CABALLO', 'BABEROS', 'DAMASCO', 'ECHANDO', 'FACETAS', 'HABITOS', 'NACIMOS'];
      this.nombreSecreto = this.palabraAleatoria(0, (this.nombres.length - 1));
      this.muestraHuecos = this.muestraHuecosPalabra();
    } else if (this.nivel == "Medio") {
      console.log(this.nivel);
      this.nombres = ['EBRIEDAD', 'ISLAMICO', 'IMAGENES', 'JABALIES', 'INACTIVO', 'IMAGINAR', 'GABACHOS', 'NACIENDO', 'OBEDEZCO', 'YANURIAS', 'BABOSEABA', 'DACTILADO', 'ECHAREMOS', 'FABRICADO', 'HABILITAR', 'LABERINTO', 'QUEBRAMOS', 'SABANDIJA', 'ZABORRERO', 'XILOFONOS'];
      this.nombreSecreto = this.palabraAleatoria(0, (this.nombres.length - 1));
      this.muestraHuecos = this.muestraHuecosPalabra();
    } else if (this.nivel == "Dificil") {
      console.log(this.nivel);
      this.nombres = ['ABACORARON', 'CABALGARON', 'DACTILARES', 'ECLIPSADOS', 'FABRICARON', 'HABICHUELA', 'PABELLONES', 'BACHATEANDO', 'DALTONIANOS', 'DAMNIFICADO', 'HABILIDOSOS', 'MACARRONEAS', 'NACIMIENTOS', 'BACHILLERATO', 'DACTILOGRAFA', 'ECLOSIONARAS', 'FABRICADORES', 'NALGUEAREMOS', 'NACIONALIZAR', 'VACACIONALES'];
      this.nombreSecreto = this.palabraAleatoria(0, (this.nombres.length - 1));
      this.muestraHuecos = this.muestraHuecosPalabra();
    }
  }

  // Método que genera una palabra aleatoria comprendida en el array nombres.	
  public palabraAleatoria(primer, ultimo) {
    let numberOfName = Math.round(Math.random() * (ultimo - primer) + (primer));
    return this.nombres[numberOfName];
  }

  // Método que valida la letra seleccionada.	
  public compruebaLetra(item) {
    // Formateamos a mayúsculas para mejorar la legibilidad.
    let letraMayusculas = item.toUpperCase();

    // Si se ha seleccionado una letra...		
    if (letraMayusculas) {

      if (this.nombreSecreto.indexOf(letraMayusculas) != -1) {

        let nombreSecretoModificado = this.nombreSecreto;
        let posicion = new Array;
        let posicionTotal = 0;

        let contador = 1;
        while (nombreSecretoModificado.indexOf(letraMayusculas) != -1) {

          posicion[contador] = nombreSecretoModificado.indexOf(letraMayusculas);
          nombreSecretoModificado = nombreSecretoModificado.substring(nombreSecretoModificado.indexOf(letraMayusculas) + letraMayusculas.length, nombreSecretoModificado.length);

          // Calculamos la posición total.
          if (contador > 1) {
            posicionTotal = posicionTotal + posicion[contador] + 1;
          }
          else {
            posicionTotal = posicionTotal + posicion[contador];
          }

          this.palabra[posicionTotal] = letraMayusculas;

          this.mensaje = 'GENIAL!!! La letra ' + letraMayusculas + ' se encuentra en la palabra secreta.';

          //Suma de puntos
          if (this.controlLetras.indexOf(letraMayusculas) == -1) {
            this.puntos = this.puntos + 10;
          }
          else {
            this.mensaje = 'La letra ' + letraMayusculas + ' fue seleccionada anteriormente';
          }

          contador++;

          // Si ya no quedan huecos, mustramos el mensaje para el ganador.
          if (this.palabra.indexOf('_') == -1) {

            // Sumamos puntos
            if (this.controlLetras.indexOf(letraMayusculas) == -1) {
              this.puntos = this.puntos + 50;
            }

            // Damos el juego por finalizado, el jugador gana.
            this.finDelJuego('gana')
          }

        }
      }
      else {
        // Restamos una vida.
        this.nuevoFallo();

        // Comprobamos si nos queda alguna vida.
        if (this.vidas > 0) {

          // Restamos puntos siempre y cuando no sean 0.
          if (this.puntos > 0) {
            if (this.controlLetras.indexOf(letraMayusculas) == -1) {
              this.puntos = this.puntos - 5;
            }
          }

          // Mostramos un mensaje indicando el fallo.					
          this.mensaje = 'Fallo, la letra ' + letraMayusculas + ' no está en la palabra secreta, recuerda que te quedan ' + this.vidas + ' vidas.';
        }
        else {
          // Damos el juego por finalizado, el jugador pierde.
          this.finDelJuego('pierde')
        }
      }

      // Añadimos al array de letras la nueva letra seleccionada.
      this.controlLetras.push(letraMayusculas);

    }
    else {
      this.mensaje = 'Seleccione una letra del listado.';
    }
  }

  public muestraHuecosPalabra() {
    let totalHuecos = this.nombreSecreto.length;

    // Declaramos la variable huecos como nuevo array.		
    let huecos = new Array;
    for (let i = 0; i < totalHuecos; i++) {
      //Asignamos tantos huecos como letras tenga la palabra.
      huecos.push('_');
    }

    // Para empezar formamos la variable palabra tan solo con los huecos, ya que en este momento aún no se ha seleccionado ninguna letra.	
    this.palabra = huecos;
    return this.palabra;
  }

  public nuevoFallo() {
    this.vidas = this.vidas - 1;    

    switch(this.vidas){
     case 7:{
      this.imagen='../assets/image/1.png'
      break;
     }
     case 6:{
       this.imagen='../assets/image/2.png'
       break;
     }
     case 5:{
       this.imagen='../assets/image/3.png'
       break
     }
     case 4:{
       this.imagen='../assets/image/4.png'
       break
     }
     case 3:{
       this.imagen='../assets/image/5.png'
       break
     }
     case 2:{
       this.imagen='../assets/image/6.png'
       break
     }
     case 1:{
       this.imagen='../assets/image/7.png'
       break
     }
     case 0:{
       this.imagen='../assets/image/8.png'
       break
     }
   }

    return this.vidas;
  }

  public finDelJuego(valor) {
    // Perdedor
    if (valor == 'pierde') {
      // Mostramos el mensaje como que el juego ha terminado
      this.mensaje = 'Perdiste!, Inténtalo de nuevo. Has conseguido un total de ' + this.puntos + ' puntos. La palabra secreta es ' + this.nombreSecreto;
      this.awaitingAjaxCall= [true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true];
    }

    // Ganador
    if (valor == 'gana') {
      this.mensaje = 'Excelente!, Has acertado la palabra secreta. Has conseguido un total de ' + this.puntos + ' puntos.';
      this.ganador = 1;
      this.awaitingAjaxCall= [true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true];
    }
  }

  public reiniciaJuego() {
    this.letra = '';
    this.palabra = '';
    this.vidas = 8;
    this.mensaje = '';
    this.ganador = 0;
    this.puntos = 0;
    this.nombreSecreto = this.palabraAleatoria(0, (this.nombres.length - 1));
    this.muestraHuecos = this.muestraHuecosPalabra();
    this.imagen = '';//'../assets/image/0.png';
    this.awaitingAjaxCall = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
  }

  getColor(event, item, row) {
    console.log(" ", item);
    console.log(" ", event);
    this.compruebaLetra(item);
    this.awaitingAjaxCall[row] = true;    
    //isEmptyCart();
    
  }

  //trigger($event){
    // $event.buttonDisabled = true;
  //}

  isEmptyCart() {        
    if (this.controlLetras.length == 0) return true; 
    return false;
  }

}
