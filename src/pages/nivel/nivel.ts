import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';

@Component({
  selector: 'page-nivel',
  templateUrl: 'nivel.html',
})
export class NivelPage { 
  nivel: string="";
  mensaje: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NivelPage');
  }
  ionEnviarNivel(){
    console.log(this.nivel);
    if (this.nivel != ""){
      this.navCtrl.push(HomePage, { nivel: this.nivel });
    }else{
      this.mensaje = 'Debe seleccionar un nivel para continuar';
    }
    
  }

}
